#package specific information for the desktop file
sed -i "/^Terminal=/i Icon=share/gimp/2.0/images/gimp-logo.png" ${INSTALL_DIR}/*.desktop
#a short description for the package
sed -i "s/@description@/gimp is an image editor/" ${INSTALL_DIR}/manifest.json

#adapt to fit the needs of the package
